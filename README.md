# Soapbox Patron

> :warning: This project is experimental and being developed in the open. **It is not production ready.** This README describes the way things will be rather than how they presently are.

Recurring donations for Fediverse servers such as Pleroma and Mastodon.
It's a Node.js microservice that runs alongside your server and processes payments using Stripe.
Soapbox Patron seeks to solve the problem of funding Fediverse servers by offering users incentives to donate.

![Soapbox Patron](https://i.imgur.com/tnAgWA9.png)

Soapbox Patron will enable users to make recurring donations to your platform by entering their credit card information.
Users may manage the donation themselves, and a funding bar may be displayed on your server.
Profile customizations are available to patrons, including the ability to set a Donor badge and profile background.

While Soapbox Patron should work with any backend, a supported frontend is needed.
You may use a supported frontend such as [Soapbox FE](https://gitlab.com/soapbox-pub/soapbox-fe), or code in support yourself.

## Installation

Soapbox Patron is a Node.js microservice that can run alongside Pleroma or Mastodon.
It exposes API endpoints on `/patron/*` that a compatible frontend can take advantage of.

To install:

1. Install Node.js, Yarn, and Postgres.
2. Clone soapbox-patron onto your production server.
3. Copy `.env.example` to `.env` and fill in your Stripe credentials.
4. Provision the Postgres database and bootstrap Stripe with initial data.
5. Run it as a service. systemd is recommended.
6. Edit your Nginx config and proxy requests to `/patron` to the service.
7. Ensure you're using a compatible frontend, and that Patron support is enabled in your frontend.

## API endpoints

To integrate Patron into your own frontend you will need to incorporate its endpoints.

### GET `/patron/v1/funding`

Public endpoint.
Returns funding data for the server.
Can be used to display a funding bar.

Example 200 response:
```json
{
  "amount": 6050,
  "amount_formatted": "$60.50",
  "patrons": 4,
  "currency": "usd",
  "interval": "monthly",
  "goals": [{
    "amount": 4500,
    "amount_formatted": "$45.00",
    "text": "Monthly server costs covered."
  }, {
    "amount": 7500,
    "amount_formatted": "$75.00",
    "text": "All operating expenses covered, including servers, media storage, email, and DDoS protection."
  }, {
    "amount": 50000,
    "amount_formatted": "$500.00",
    "text": "@alex will be able to work part-time implementing new features. Thank you for your generosity!"  
  }]
}
```

### GET `/patron/v1/accounts/:id`

Public endpont.
Returns Patron-related metadata about the given Account.
The ID must be a valid Account ID in the backend service.

Example 200 response:
```json
{
  "id": "9tSK9gZfIRaCbMN3ey",
  "is_patron": true,
  "customizations": {
    "background": "floral"
  }
}
```

### GET `/patron/v1/me`

Requires `Authorization` HTTP header.
Returns plan (if any) and payment history for the given user.

Example 200 response:
```json
{
  "plan": {
    "amount": 500,
    "amount_formatted": "$5.00",
    "currency": "usd",
    "interval": "monthly",
    "created": 1583792424,
    "expires": 1586715018
  },
  "history": [{
    "type": "charge",
    "amount": 500,
    "amount_formatted": "$5.00",
    "created": 1583792424
  }]
}
```

### POST `/patron/v1/plan`

Requires `Authorization` HTTP header.
Creates a new plan for the user.
Returns 409 (conflict) if the user already has a plan.

Request parameters:

- `amount` Number
- `currency` String ("usd")
- `interval` String ("monthly")
- `payment` Object

The `payment` object looks like this:
```json
{
  "type": "stripe",
  "source": "tok_xxx"
}
```

The token must be generated on the frontend using [Stripe.js](https://stripe.com/docs/stripe-js).
Credit card data is sent directly from the user's browser to Stripe's server, and we perform logic on the token received in return.

Example 200 response (it returns the plan):
```json
{
  "amount": 500,
  "amount_formatted": "$5.00",
  "currency": "usd",
  "interval": "monthly",
  "created": 1583792424,
  "expires": 1586715018
}
```

### PATCH `/patron/v1/plan`

Requires `Authorization` HTTP header.
Updates the user's plan with the given parameters.

Request parameters: Same as POST `/patron/v1/plan`, but all optional.

200 response: Same as POST `/patron/v1/plan`.

### DELETE `/patron/v1/plan`

Requires `Authorization` HTTP header.
Deletes the user's plan.

### PUT `/patron/v1/goals`
Requires `Authorization` HTTP header.
The authorized user must be an admin.
Updates the site goals.

# License

Soapbox Patron is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Soapbox Patron is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Soapbox Patron.  If not, see <https://www.gnu.org/licenses/>.
