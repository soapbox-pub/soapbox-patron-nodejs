const config = require('../config.json');

exports.getGoals = () => {
  return config.goals;
}
