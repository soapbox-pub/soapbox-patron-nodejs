const { BACKEND_URL } = process.env;

exports.authAdmin = (req, res, next) => {
  const account = res.locals.account;
  // FIXME: This way only works with Pleroma
  if (account && account.pleroma && account.pleroma.is_admin) {
    return next();
  }
  return res.sendStatus(403);
}

// Middleware to set the current user if any
exports.loginUserMiddleware = (req, res, next) => {
  const auth = req.get('Authorization');
  if (!auth) return next();
  axios.get(`${BACKEND_URL}/api/v1/accounts/verify_credentials`, {
    headers: {'Authorization': auth}
  }).then(_res => {
    res.locals.account = _res.data;
    next();
  }).catch(e => {
    console.error(e);
    next();
  });
}
