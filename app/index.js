require('dotenv').config();
const { PATRON_PORT, PATRON_HOST } = process.env;

const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const app = express();

const { authAdmin, loginUserMiddleware } = require('./auth');
const { getFunding } = require('./funding');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(loginUserMiddleware);

app.get('/patron/v1/funding', (req, res) => {
  getFunding().then(funding => {
    res.json(funding);
  }).catch(err => {
    console.error(err);
    return res.send(500);
  });
});

app.listen(PATRON_PORT, () => {
  let url = `http://${PATRON_HOST}:${PATRON_PORT}`;
  console.log(`Soapbox Patron running on ${url}`);
});
