const { getStripeData } = require('./stripe');
const { getGoals } = require('./goals');

exports.getFunding = () => {
  return Promise.all([getStripeData(), getGoals()])
  .then((values) => {
    const [data, goals] = values;
    // FIXME: Cache this result
    return {
      amount: data.amount,
      patrons: data.patrons,
      currency: 'usd',
      interval: 'monthly',
      goals: goals
    };
  });
}
