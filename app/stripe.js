const { STRIPE_SECRET_KEY } = process.env;
const stripe = require('stripe')(STRIPE_SECRET_KEY);

exports.getStripeData = () => {
  let data = {amount: 0, patrons: 0};
  return stripe.subscriptions.list({plan: "plan_monthly_donation"})
  .autoPagingEach(subscription => {
    data.amount += subscription.quantity;
    data.patrons++;
  }).then(() => {
    return data;
  });
}
